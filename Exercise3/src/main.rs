use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for i in 0..N {
        children.push(thread::spawn(move || {
            println!("This is thread number {}", i);
        }));
    }

    for child in children {
        let _ = child.join();
    }
}
