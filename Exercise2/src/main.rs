// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut fib1 = 0;
    let mut fib2 = 1;

    for i in 2..n+1 {
        let temp = fib1;
        fib1 = fib2;
        fib2 += temp;
    }

    return fib2;
}


fn main() {
    println!("{}", fibonacci_number(10));
}
